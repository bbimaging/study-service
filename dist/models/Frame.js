"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Frame = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const frameSchema = new mongoose_1.default.Schema({
    study: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: "Study"
    },
    url: String,
    description: { type: String },
    annotations: Array
}, { timestamps: true });
exports.Frame = mongoose_1.default.model("Frame", frameSchema);
//# sourceMappingURL=Frame.js.map