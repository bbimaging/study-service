"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Study = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const studySchema = new mongoose_1.default.Schema({
    name: { type: String },
}, { timestamps: true });
exports.Study = mongoose_1.default.model("Study", studySchema);
//# sourceMappingURL=Study.js.map