"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.send = void 0;
const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-west-2' });
const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });
const queueUrl = "https://sqs.us-west-2.amazonaws.com/791342033319/FRAME_READY";
const send = (data) => __awaiter(void 0, void 0, void 0, function* () {
    let sendSqsMessage = sqs.sendMessage({
        MessageBody: data,
        QueueUrl: queueUrl
    }).promise();
    sendSqsMessage.then((data) => {
        console.log('data=', data);
        console.log(`SUCCESS: ${data.MessageId}`);
    }).catch((err) => {
        console.error(err);
    });
});
exports.send = send;
//# sourceMappingURL=producer.js.map