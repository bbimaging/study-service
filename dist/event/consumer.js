"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Frame_1 = require("../models/Frame");
const Producer = __importStar(require("./producer"));
const AWS = require('aws-sdk');
const { Consumer } = require('sqs-consumer');
AWS.config.update({ region: 'us-west-2' });
const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });
const FRAME_CREATED_URL = "https://sqs.us-west-2.amazonaws.com/791342033319/FRAME_CREATED";
const FRAME_UPDATED_URL = "https://sqs.us-west-2.amazonaws.com/791342033319/FRAME_UPDATED";
const URL_PREFIX = "https://bbimaging-0.s3.us-west-2.amazonaws.com/";
const frameCreated = Consumer.create({
    queueUrl: FRAME_CREATED_URL,
    handleMessage: (message) => __awaiter(void 0, void 0, void 0, function* () {
        const key = JSON.parse(message.Body).Records[0].s3.object.key;
        const frame = new Frame_1.Frame({
            study: key.split("/")[0],
            url: URL_PREFIX.concat(key),
            key: key,
            description: "",
            annotations: []
        });
        yield frame.save();
        Producer.send(JSON.stringify({ key }));
        removeFromQueue(FRAME_CREATED_URL, message);
        console.log(frame);
    }),
    sqs: new AWS.SQS()
}).start();
const frameUpdated = Consumer.create({
    queueUrl: FRAME_UPDATED_URL,
    handleMessage: (message) => __awaiter(void 0, void 0, void 0, function* () {
        const key = JSON.parse(message.Body);
        console.log(message.Body);
        removeFromQueue(FRAME_UPDATED_URL, message);
    }),
    sqs: new AWS.SQS()
}).start();
const removeFromQueue = function (url, message) {
    sqs.deleteMessage({
        QueueUrl: url,
        ReceiptHandle: message.ReceiptHandle
    }, function (err, data) {
        err && console.log(err);
    });
};
//# sourceMappingURL=consumer.js.map