"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFrame = exports.getFrame = void 0;
const Frame_1 = require("../models/Frame");
const getFrame = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.send(yield Frame_1.Frame.find({ study: req.params.studyId }));
});
exports.getFrame = getFrame;
const createFrame = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const frame = new Frame_1.Frame({
        study: req.body.study,
        description: req.body.description,
        annotations: req.body.annotations
    });
    const newFrame = yield frame.save();
    res.send(newFrame);
});
exports.createFrame = createFrame;
//# sourceMappingURL=frame.js.map