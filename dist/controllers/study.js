"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStudy = exports.getOne = exports.getAll = void 0;
const Study_1 = require("../models/Study");
const getAll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.send(yield Study_1.Study.find());
});
exports.getAll = getAll;
const getOne = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let response = {};
    if (req.query.frames !== undefined) {
        response = { study: yield Study_1.Study.findById(req.params.id).exec(), frames: [] };
    }
    else {
        response = yield Study_1.Study.findById(req.params.id).exec();
    }
    res.send(response);
});
exports.getOne = getOne;
const createStudy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const study = new Study_1.Study({ name: req.body.name });
    const newStudy = yield study.save();
    res.send(newStudy);
});
exports.createStudy = createStudy;
//# sourceMappingURL=study.js.map