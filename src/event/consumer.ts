import { SQSMessage } from "sqs-consumer";
import { Frame } from "../models/Frame";
import * as Producer from "./producer"

const AWS = require('aws-sdk');
const { Consumer } = require('sqs-consumer');

AWS.config.update({region: 'us-west-2'});

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});
const FRAME_CREATED_URL = "https://sqs.us-west-2.amazonaws.com/791342033319/FRAME_CREATED";
const FRAME_UPDATED_URL = "https://sqs.us-west-2.amazonaws.com/791342033319/FRAME_UPDATED";
const URL_PREFIX = "https://bbimaging-0.s3.us-west-2.amazonaws.com/";

const frameCreated = Consumer.create({
    queueUrl: FRAME_CREATED_URL,
    handleMessage: async (message: SQSMessage) => {
        const key = JSON.parse(message.Body).Records[0].s3.object.key;
        const frame = new Frame({
            study: key.split("/")[0],
            url: URL_PREFIX.concat(key),
            key: key,
            description: "",
            annotations: []
        });

        await frame.save();

        Producer.send(JSON.stringify({_id: frame._id, key:key}));

        removeFromQueue(FRAME_CREATED_URL, message);
    },
    sqs: new AWS.SQS()
}).start();

const frameUpdated = Consumer.create({
    queueUrl: FRAME_UPDATED_URL,
    handleMessage: async (message: SQSMessage) => {
        const body = JSON.parse(message.Body);
        const _id = body['_id'];
        const description = body['description'];

        const frame = await Frame.findById(_id);
        frame.description = description;
        await frame.save();
        console.log(frame);

        removeFromQueue(FRAME_UPDATED_URL, message);
    },
    sqs: new AWS.SQS()
}).start();

const removeFromQueue = function(url: string, message: SQSMessage) {
    sqs.deleteMessage({
        QueueUrl: url,
        ReceiptHandle : message.ReceiptHandle
    }, function(err: any, data: any) {
        err && console.log(err);
    });
};