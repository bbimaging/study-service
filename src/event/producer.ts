const AWS = require('aws-sdk');

AWS.config.update({region: 'us-west-2'});

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});
const queueUrl = "https://sqs.us-west-2.amazonaws.com/791342033319/FRAME_READY";

export const send = async (data: string) => {
    let sendSqsMessage = sqs.sendMessage({
        MessageBody: data,
        QueueUrl: queueUrl
    }).promise();
    
    sendSqsMessage.then((data: { MessageId: any; }) => {
        console.log('data=', data);
        console.log(`SUCCESS: ${data.MessageId}`);
    }).catch((err: any) => {
        console.error(err)
    });
};