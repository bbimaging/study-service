import express from "express";
import session from "express-session";
import bodyParser from "body-parser";
import MongoStore from "connect-mongo";
import mongoose from "mongoose";
import bluebird from "bluebird";

require("./event/consumer");

import * as study from "./controllers/study";
import * as frame from "./controllers/frame";

const cors = require('cors')
const app = express();
const mongoUrl = process.env["MONGODB_URI"] || "mongodb://localhost:27017/bbimaging";
mongoose.Promise = bluebird;
mongoose.connect(mongoUrl, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true } ).then(() => {},
).catch(err => console.error(`mongodb error: ${err}`));

app.set("port", process.env.PORT || 8080);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    resave: true,                     
    saveUninitialized: true,
    secret: process.env["SESSION_SECRET"] || "abc123",
    store: new MongoStore({
        mongoUrl,
        mongoOptions: {
            autoReconnect: true
        }
    })
}));

app.get("/study", study.getAll);
app.get("/study/:id", study.getOne);
app.post("/study", study.createStudy);

app.post("/frame", frame.createFrame);
app.get("/frame/study/:studyId", frame.getFrame);

export default app;