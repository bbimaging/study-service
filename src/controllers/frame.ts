import { Request, Response } from "express";
import { Frame } from "../models/Frame";

export const getFrame = async (req: Request, res: Response) => {
    res.send(await Frame.find({study:req.params.studyId}));
};

export const createFrame = async (req: Request, res: Response) => {
    const frame = new Frame({
        study: req.body.study,
        description: req.body.description,
        annotations: req.body.annotations
    });
    const newFrame = await frame.save();
    res.send(newFrame);
};