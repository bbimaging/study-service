import { Request, response, Response } from "express";
import { Study } from "../models/Study";

export const getAll = async (req: Request, res: Response) => {
    res.send(await Study.find());
};

export const getOne = async (req: Request, res: Response) => {
    let response = {};
    if(req.query.frames !== undefined) {
        response = { study: await Study.findById(req.params.id).exec(), frames:[] };
    } else {
        response = await Study.findById(req.params.id).exec();
    }
    res.send(response);
};

export const createStudy = async (req: Request, res: Response) => {
    const study = new Study({name:req.body.name});
    const newStudy = await study.save();
    res.send(newStudy);
};