import { ObjectId } from "bson";
import mongoose from "mongoose";

interface Coordinate {
    x1: number;
    y1: number;
    x2: number;
    y2: number;
}

interface Annotation {
    template: string;
    toolType: string;
    coordinates: Coordinate;
}

export type FrameDocument = mongoose.Document & {
    study: ObjectId,
    url: string,
    description: string;
    annotations: Annotation[];
};

const frameSchema = new mongoose.Schema<FrameDocument>(
    {
        study :{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Study"
        },
        url: String,
        description: { type: String },
        annotations: Array
    },
    { timestamps: true },
);

export const Frame = mongoose.model<FrameDocument>("Frame", frameSchema);