import mongoose from "mongoose";

export type StudyDocument = mongoose.Document & {
    name: string;
};

const studySchema = new mongoose.Schema<StudyDocument>(
    {
        name: { type: String },
    },
    { timestamps: true },
);

export const Study = mongoose.model<StudyDocument>("Study", studySchema);